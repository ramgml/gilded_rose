<?php

namespace GildedRose;

use GildedRose\Products\BackstagePassesProduct;
use GildedRose\Products\ConjuredProduct;
use GildedRose\Products\DefaultProduct;
use GildedRose\Products\AgedBrieProduct;
use GildedRose\Products\SulfurasProduct;

/**
 * Class GildedRose
 */
class GildedRose
{

    private $items;

    /**
     * GildedRose constructor.
     *
     * @param $items
     */
    function __construct($items)
    {
        $this->items = $items;
    }

    function updateQuality()
    {
        foreach ($this->items as $item) {
            switch ($item->name) {
                case 'Aged Brie':
                    $product = new AgedBrieProduct($item);
                    break;
                case 'Backstage passes to a TAFKAL80ETC concert':
                    $product = new BackstagePassesProduct($item);
                    break;
                case 'Sulfuras, Hand of Ragnaros':
                    $product = new SulfurasProduct($item);
                    break;
                case 'Conjured':
                    $product = new ConjuredProduct($item);
                    break;
                default:
                    $product = new DefaultProduct($item);
            }
            $product->update();
        }
    }
}
