<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 7:31 PM
 */

namespace GildedRose\Products;

use GildedRose\Item;

/**
 * Class AbstractProduct
 *
 * @package GildedRose\Products
 */
abstract class AbstractProduct
{
    /**
     * @var Item
     */
    protected $item;

    /**
     * @var int
     */
    protected $maxQuality = 50;

    /**
     * AbstractProduct constructor.
     *
     * @param Item $item
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    /**
     *  Minimal value of quality is 0
     */
    protected function roundQuality()
    {
        $this->item->quality = $this->item->quality > 0
            ? $this->item->quality : 0;
    }

    /**
     * Reduce shelf life
     */
    protected function calculateSellIn()
    {
        $this->item->sell_in--;
    }

    /**
     * Handle item
     *
     * @return void
     */
    public function update()
    {
        $this->calculateQuality();
        $this->roundQuality();
        $this->calculateSellIn();
    }

    /**
     * @return mixed
     */
    abstract protected function calculateQuality();
}