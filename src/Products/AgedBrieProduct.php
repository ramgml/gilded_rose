<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 8:23 PM
 */

namespace GildedRose\Products;

/**
 * Class AgedBrieProduct
 *
 * @package GildedRose\Products
 */
class AgedBrieProduct extends AbstractProduct
{
    /**
     * If the quality is less than the maximum then add 1
     *
     * @return void
     */
    protected function calculateQuality()
    {
        $quality = $this->item->quality;
        $max = $this->maxQuality;
        $this->item->quality = $quality < $max ? $quality + 1 : $max;
    }
}