<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 10:23 PM
 */

namespace GildedRose\Products;

/**
 * Class BackstagePassesProduct
 *
 * @package GildedRose\Products
 */
class BackstagePassesProduct extends AbstractProduct
{
    /**
     * Quality increases by 2 when there are 10 days or less
     * and by 3 when there are 5 days or less but
     * Quality drops to 0 after the concert
     *
     * @return void
     */
    protected function calculateQuality()
    {
        $quality = $this->item->quality;
        $max = $this->maxQuality;
        switch (true) {
            case $this->item->sell_in < 0:
                $quality = 0;
                break;
            case $this->item->sell_in <= 5:
                $quality = $quality < $max ? $quality + 3 : $max;
                break;
            case $this->item->sell_in <= 10:
                $quality = $quality < $max ? $quality + 2 : $max;
                break;
            default:
                $quality = $quality < $max ? $quality + 1 : $max;
        }
        $this->item->quality = $quality;
    }
}