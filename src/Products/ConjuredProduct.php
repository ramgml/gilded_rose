<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 10:58 PM
 */

namespace GildedRose\Products;

/**
 * Class ConjuredProduct
 *
 * @package GildedRose\Products
 */
class ConjuredProduct extends AbstractProduct
{
    /**
     * Conjured products quality
     *
     * @return void
     */
    protected function calculateQuality()
    {
        if ($this->item->sell_in < 0) {
            $this->item->quality -= 4;
        } else {
            $this->item->quality -= 2;
        }
    }
}