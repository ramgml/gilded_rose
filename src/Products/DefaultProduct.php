<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 7:58 PM
 */

namespace GildedRose\Products;

/**
 * Class DefaultProduct
 *
 * @package GildedRose\Products
 */
class DefaultProduct extends AbstractProduct
{
    /**
     * Default products quality
     *
     * @return void
     */
    protected function calculateQuality()
    {
        if ($this->item->sell_in < 0) {
            $this->item->quality -= 2;
        } else {
            $this->item->quality--;
        }
    }
}