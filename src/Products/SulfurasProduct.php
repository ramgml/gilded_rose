<?php
/**
 * Created by PhpStorm.
 * User: yutuk
 * Date: 12/5/2018
 * Time: 10:18 PM
 */

namespace GildedRose\Products;

/**
 * Class SulfurasProduct
 *
 * @package GildedRose\Products
 */
class SulfurasProduct extends AbstractProduct
{
    protected $maxQuality = 80;

    /**
     * Quality unchanged
     *
     * @return void
     */
    protected function calculateQuality()
    {
        $this->item->quality = $this->maxQuality;
    }

    /**
     * Sell in does not change
     */
    protected function calculateSellIn()
    {
        return;
    }
}