<?php

namespace Tests;


use GildedRose\GildedRose;
use GildedRose\Item;

class GildedRoseTest extends \PHPUnit_Framework_TestCase
{
    public function testAgedBrei()
    {
        $items = [
            new Item("Aged Brie", 10, 2),
            new Item("Aged Brie", 1, 50),
        ];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(9, $items[0]->sell_in);
        $this->assertEquals(3, $items[0]->quality);
        $this->assertEquals(0, $items[1]->sell_in);
        $this->assertEquals(50, $items[1]->quality);
    }

    public function testBackstagePasses()
    {
        $items = [
            new Item("Backstage passes to a TAFKAL80ETC concert", 12, 2),
            new Item("Backstage passes to a TAFKAL80ETC concert", 8, 4),
            new Item("Backstage passes to a TAFKAL80ETC concert", 3, 5),
            new Item("Backstage passes to a TAFKAL80ETC concert", -1, 6),
        ];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(3, $items[0]->quality);
        $this->assertEquals(6, $items[1]->quality);
        $this->assertEquals(8, $items[2]->quality);
        $this->assertEquals(0, $items[3]->quality);
    }

    public function testConjured()
    {
        $items = [
            new Item("Conjured", 12, 2),
            new Item("Conjured", -1, 4),
        ];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(0, $items[0]->quality);
        $this->assertEquals(11, $items[0]->sell_in);
        $this->assertEquals(0, $items[1]->quality);
    }

    public function testDefault()
    {
        $items = [
            new Item("Foo", 12, 2),
            new Item("Foo", 0, 0),
            new Item("Foo", -3, 5),
        ];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(1, $items[0]->quality);
        $this->assertEquals(11, $items[0]->sell_in);
        $this->assertEquals(0, $items[1]->quality);
        $this->assertEquals(3, $items[2]->quality);
    }

    public function testSulfuras()
    {
        $items = [
            new Item("Sulfuras, Hand of Ragnaros", 12, 80),
        ];
        $gildedRose = new GildedRose($items);
        $gildedRose->updateQuality();
        $this->assertEquals(80, $items[0]->quality);
        $this->assertEquals(12, $items[0]->sell_in);
    }
}
